(ns plf08.core-test
  (:require [clojure.test :refer [deftest is testing]]
            [plf08.core :refer [función-cifrado-cup función-decifrate-2 función-decifrate-1 función-cifrar función-descifrar]]
            [plf08.columnarsimple :refer [orden-char ordenar-valores-posiciones obtener-posiciones-2 agrupar-texto cifrar-1 cifrar-descifrar pos-orden ordenar-texto-cif num-particiones cifrar-texto descifrar-texto]]))

(deftest cifrado-cup-test
  (testing "Cifrar palabras que solo contengan letras minusculas"
    (is (= "♞♖♜♜♞♘♝♖♜♖♞♚" (función-cifrado-cup "manuel")))
    (is (= "♝♜♝♝♞♕♜♕♝♝♜♜♞♖♜♜♜♛♞♜♞♔♞♘" (función-cifrado-cup "programación"))))
  (testing "Cifrar oraciones que contengan mayusculas, minusculas, simbolos, numeros y espacios"
    (is (= "♕.♞♞♜♛♝♚♞♕♝♝  ♔♜♜♜♞♘♝♖♜♖♞♚  ♕.♜♞♝♛♝♞♝♖♜♖♛♝  ♔♕♜♜♞♚♞♜♞♘♜♜♝♛" (función-cifrado-cup "Víctor Manuel Vásquez Salinas")))
    (is (= "♕♖♞♘♜♕♜♖♞♘♞♜♜♖♝♝♞♞♜♜  ♕♜♞♘  ♔♕♞♜♝♛♝♚♜♖♞♖♜♜♝♛  ♗♕♞♕♞♖♝♜♝♖♝♚♜♜♜♛♞♜♞♕♞♘♜♜♞♚♜♖♝♛" (función-cifrado-cup "Ingeniería En Sistemas Computacionales")))
    (is (= "♛♚♗♞♗♚♛♖♛♛♛♗♛♛♗♜♖♔♜♕♞♖♜♜♞♜♞♚♖♞♞♜♝♚♞♕♜♜♛♜♜♜♜♛♜♜♖♞♜♖♜♚♝♖♖♞♞♖♛♜" (función-cifrado-cup "16920405@gmail.itoaxaca.edu.mx"))))
  (testing "Cifrar palabras que contengan caracteres de las coordenadas"
    (is (= "♗♕♞♕♞♕♝♝♜♚♜♖♞♘♜♜♜♚♜♜ ♚" (función-cifrado-cup "Coordenada♚"))))
  (testing "Cifrar parrafos para verificar los saltos de lineas"
    (is (= "♗♕♔♛♔♗♔♛\n ♔♔♜♖♜♛♞♘♞♕♞♚♞♔♜♕♞♜♜♛♞♕♚♔  ♞♚♝♖♛♝  ♜♚♜♖  ♞♚♜♜  ♜♛♞♜♜♖♞♘♜♛♞♜♜♜♖♚\n ♝♜♞♚♜♜♝♚♜♜♜♗♞♕♝♝♞♖♜♜  ♝♜♜♜♝♝♜♜  ♜♖♞♚  ♝♜♞♕♝♝♝♕♜♖♞♘♞♜♝♝♖♚\n ♜♛♞♕♞♘  ♞♚♜♜  ♝♚♜♘♜♛♞♘♞♜♜♛♜♜  ♜♗♞♕♝♝♞♝♜♜♝♛  ♜♛♞♕♞♘♜♛♞♜♜♖♞♘♜♛♞♜♜♜\n ♝♜♜♜♝♝♜♜  ♜♚♜♜♝♝♞♘♞♕♝♛  ♜♗♜♖♞♚♞♜♛♝  ♜♚♜♖♝♕♜♖♞♘♞♜♝♝♖♞"
           (función-cifrado-cup "CORO
Tecnológico, luz de la ciencia;
plataforma para el porvenir;
con la técnica forjas conciencia
para darnos feliz devenir.")))))

(deftest descifrado-un-elemento-test
  (testing "Descifrar una letra minuscula"
    (is (= \a (función-decifrate-1 "♜♜")))
    (is (= \ó (función-decifrate-1 "♞♔"))))
  (testing "Descifrar una letra mayuscula"
    (is (= \T (función-decifrate-1 "♔♔")))
    (is (= \Í (función-decifrate-1 "♕♘"))))
  (testing "Descifrar un simbolo"
    (is (= \? (función-decifrate-1 "♖♕")))
    (is (= \+ (función-decifrate-1 "♚♕"))))
  (testing "Descifrar un numero"
    (is (= \5 (función-decifrate-1 "♗♜")))
    (is (= \0 (función-decifrate-1 "♛♛")))))

(deftest descifrado-decifrate-2-test
  (testing "Descifrar palabras que solo contengan minusculas"
    (is (= '(\m \a \n \u \e \l) (función-decifrate-2 (partition-all 2 "♞♖♜♜♞♘♝♖♜♖♞♚"))))
    (is (= '(\p \r \o \g \r \a \m \a \c \i \ó \n) (función-decifrate-2 (partition-all 2 "♝♜♝♝♞♕♜♕♝♝♜♜♞♖♜♜♜♛♞♜♞♔♞♘")))))
  (testing "Descifrar oraciones que contengan mayusculas, minusculas, simbolos, numeros y espacios"
    (is (= '(\V \í \c \t \o \r \space \M \a \n \u \e \l \space \V \á \s \q \u \e \z \space \S \a \l \i \n \a \s) (función-decifrate-2 (partition 2 "♕.♞♞♜♛♝♚♞♕♝♝  ♔♜♜♜♞♘♝♖♜♖♞♚  ♕.♜♞♝♛♝♞♝♖♜♖♛♝  ♔♕♜♜♞♚♞♜♞♘♜♜♝♛"))))
    (is (= '(\I	\n	\g	\e	\n	\i	\e	\r	\í	\a	\space	\E	\n	\space	\S	\i	\s	\t	\e	\m	\a	\s	\space	\C	\o	\m	\p	\u	\t	\a	\c	\i	\o	\n	\a	\l	\e	\s)
           (función-decifrate-2 (partition 2 "♕♖♞♘♜♕♜♖♞♘♞♜♜♖♝♝♞♞♜♜  ♕♜♞♘  ♔♕♞♜♝♛♝♚♜♖♞♖♜♜♝♛  ♗♕♞♕♞♖♝♜♝♖♝♚♜♜♜♛♞♜♞♕♞♘♜♜♞♚♜♖♝♛"))))
    (is (= '(\1 \6 \9 \2 \0 \4 \0 \5 \@ \g \m \a \i \l \. \i \t \o \a \x \a \c \a \. \e \d \u \. \m \x) (función-decifrate-2 (partition 2 "♛♚♗♞♗♚♛♖♛♛♛♗♛♛♗♜♖♔♜♕♞♖♜♜♞♜♞♚♖♞♞♜♝♚♞♕♜♜♛♜♜♜♜♛♜♜♖♞♜♖♜♚♝♖♖♞♞♖♛♜")))))
  (testing "Descifrar palabras cifradas que no contengan caracteres de coordenadas"
    (is (= '(\m \a \n \u \e \l "SA") (función-decifrate-2 (partition 2 "♞♖♜♜♞♘♝♖♜♖♞♚SA")))))
  (testing "Descifrar oraciones con salto de linea"
    (is (= '(\T	\e	\c	\newline	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\space	\O	\a	\x	\a	\c	\a)
           (función-decifrate-2 (partition 2 "♔♔♜♖♜♛\n                                               ♔♛♜♜♛♜♜♜♜♛♜♜"))))))

(deftest cifrar-txt-test
  (testing "Cifrar el contenido de un archivo de texo y guardar el cifrado en otro"
    (is (nil? (función-cifrar "resources/ITO.txt" "resources/itociftest.txt")))))
(deftest descifrar-txt-test
  (testing "Descifrar el contenido de un archivo te texto y guradar el resultado en otro"
    (is (nil? (función-descifrar  "resources/itociftest.txt" "resources/itodeciftest.txt")))))

;descifrar

(deftest orden-char-test
  (testing "Obtener prioridad de caracteres"
    (is (= 1 (orden-char \a)))
    (is (= 35 (orden-char \2)))
    (is (= 49 (orden-char \+)))))

(deftest ordenar-val-pos-test
  (testing "Obtener prioridades de letras de una palbra clave"
    (is (= '([1 1] [0 4] [3 4] [4 11] [2 17] [6 17] [5 20]) (ordenar-valores-posiciones "canción")))
    (is (= '([1 1] [0 4] [3 4] [4 11] [2 17] [6 17] [5 20]) (ordenar-valores-posiciones "CANCIÓN")))
    (is (= '([1 1] [0 4] [3 4] [4 11] [2 17] [6 17] [5 20]) (ordenar-valores-posiciones "cAnCiÓn")))))

(deftest obtener-posiciones-test
  (testing "Ordenar posiciones segun prioridad"
    (is (= '(1 0 3 4 2 6 5) (obtener-posiciones-2 "canción")))
    (is (= '(1 0 3 4 2 6 5) (obtener-posiciones-2 "CANCIÓN")))
    (is (= '(1 0 3 4 2 6 5) (obtener-posiciones-2 "cAnCiÓn")))
    (is (= '(1 4 5 0 2 3) (obtener-posiciones-2 "Manuel")))))

(deftest agrupar-txt-test
  (testing "Dividir texto a cifrar en grupos iguales a la longitud de la palabra clave"
    (is (= '((\E \s \t \a \space \e \s)
             (\space \u \n \a \space \p \r)
             (\u \e \b \a \space \d \e)
             (\space \t \e \x \t \o \space)
             (\p \a \r \a \space \d \i)
             (\v \i \d \i \r)) (agrupar-texto "canción" "Esta es una prueba de texto para dividir")))
    (is (= '((\E \s \t \a \space \e \s)
             (\space \u \n \a \space \p \r)
             (\u \e \b \a \space \d \e)
             (\space \t \e \x \t \o \space)
             (\p \a \r \a \space \d \i)
             (\v \i \d \i \r)) (agrupar-texto "CANCIÓN" "Esta es una prueba de texto para dividir")))
    (is (= '((\E \s \t \a \space \e \s)
             (\space \u \n \a \space \p \r)
             (\u \e \b \a \space \d \e)
             (\space \t \e \x \t \o \space)
             (\p \a \r \a \space \d \i)
             (\v \i \d \i \r)) (agrupar-texto "cAnCiÓn" "Esta es una prueba de texto para dividir")))
    (is (= '((\M \i \space \n \o \m)
             (\b \r \e \space \e \s)
             (\space \V \í \c \t \o)
             (\r \space \M \a \n \u)
             (\e \l \space \V \a \s)
             (\q \u \e \z \space \S)
             (\a \l \i \n \a \s)) (agrupar-texto "Manuel" "Mi nombre es Víctor Manuel Vasquez Salinas")))))

(deftest cifrar-prev-test
  (testing "Agrupamiento del texto a cifrar segun las columnas de prioridad de la palabra clave"
    (is (= '((\i \V \t \o \c \space \r)
             (\a \M \u \e \n \space \l)
             (\a \V \q \u \s \z \e)
             (\S \space \l \i \a \a \n)
             (" " \s " " " " " " " " " ")) (cifrar-1 (agrupar-texto "canción" "Victor Manuel Vasquez Salinas") (obtener-posiciones-2 "canción"))))
    (is (= '((\n \i \t \I \s \t)
             (\t \T \e \u \o \space)
             (\n \ó \g \c \o \l)
             (\c \d \e \i \o \space)
             (\O \a \c \space \a \x)
             (" " " " " " \a " " " ")) (cifrar-1 (agrupar-texto "Manuel" "Instituto Tecnológico de Oaxaca") (obtener-posiciones-2 "Manuel"))))))

(deftest cifrar-test
  (testing "Cifrar un texto"
    (is (= "ntncO iTóda tegec Iuci asoooa t l x " (cifrar-descifrar "Manuel" (cifrar-1 (agrupar-texto "Manuel" "Instituto Tecnológico de Oaxaca") (obtener-posiciones-2 "Manuel")))))
    (is (= "rcgFaPaó ngócn rnac oiiulmLyo a  i " (cifrar-descifrar "canción" (cifrar-1 (agrupar-texto "canción" "Programación Lógica y Funcional") (obtener-posiciones-2 "canción")))))
    (is (= " eceb aua4i xJO1c aradm dza r cáx e a   i Oe,.de" (cifrar-descifrar "8-letras" (cifrar-1 (agrupar-texto "8-letras" "Oaxaca de Juárez, Oaxaca. 14 de diciembre") (obtener-posiciones-2 "8-letras")))))))

(deftest pos-orden-test
  (testing "Obtener las posiciones originales de las letras de palabra clave de la coleccion donde se ordenaron las letras por prioridad por prioridad"
    (is (= '(1 0 4 2 3 6 5) (pos-orden "canción")))
    (is (= '(1 0 4 2 3 6 5) (pos-orden "CANCIÓN")))
    (is (= '(1 0 4 2 3 6 5) (pos-orden "cAnCiÓn")))
    (is (= '(7 6 2 1 5 3 0 4) (pos-orden "8-letras")))))

(deftest orden-txt-cif-test
  (testing "Ordenar las divisiones del texto cifrado"
    (is (= '((\I \u \c \i \space \a)
             (\n \t \n \c \O \space)
             (\s \o \o \o \a \space)
             (\t \space \l \space \x \space)
             (\i \T \ó \d \a \space)
             (\t \e \g \e \c \space)) (ordenar-texto-cif "ntncO iTóda tegec Iuci asoooa t l x " 6 "Manuel")))
    (is (= '((\P \a \ó \space \n)
             (\r \c \g \F \a)
             (\o \i \i \u \l)
             (\g \ó \c \n \space)
             (\r \n \a \c \space)
             (\a \space \space \i \space)
             (\m \L \y \o \space)) (ordenar-texto-cif "rcgFaPaó ngócn rnac oiiulmLyo a  i " 5 "canción")))
    (is (= '((\O \e \, \. \d \e)
             (\a \space \space \space \i \space)
             (\x \J \O \1 \c \space)
             (\a \u \a \4 \i \space)
             (\c \á \x \space \e \space)
             (\a \r \a \d \m \space)
             (\space \e \c \e \b \space)
             (\d \z \a \space \r \space)) (ordenar-texto-cif " eceb aua4i xJO1c aradm dza r cáx e a   i Oe,.de" 6 "8-letras")))))

(deftest descifra-test
  (testing "Descifar un texto"
    (is (= "Instituto Tecnológico de Oaxaca     " (cifrar-descifrar "Manuel" (ordenar-texto-cif  "ntncO iTóda tegec Iuci asoooa t l x " 6 "Manuel"))))
    (is (= "Programación Lógica y Funcional    " (cifrar-descifrar "canción" (ordenar-texto-cif  "rcgFaPaó ngócn rnac oiiulmLyo a  i " 5 "canción"))))
    (is (= "Oaxaca de Juárez, Oaxaca. 14 de diciembre       " (cifrar-descifrar "8-letras" (ordenar-texto-cif  " eceb aua4i xJO1c aradm dza r cáx e a   i Oe,.de" 6 "8-letras"))))))

(deftest num-particiones-test
  (testing "Sabers cuantas divisiones se hicieron al texto original que se descifro"
    (is (= 5 (num-particiones 7 29)))))

(deftest cifrar-from-txt-test
  (testing "Cifrar un texto contenido en un archivo y guardarlo en otro"
    (is (= nil (cifrar-texto "canción" (slurp "resources/columnar1.txt") "resources/colcif.txt")))))

(deftest descifrar-from-txt-tes
  (testing "descifrar contenido de un archivo de texto y guardarlo en otro"
    (is (= nil (descifrar-texto "canción" (slurp "resources/colcif.txt") "resources/coldecif.txt")))))