(ns plf08.core
  (:gen-class) (:require plf08.columnarsimple clojure.string))

(defn función-coordy [caracter] (let [y (find
                                         {\a "♜" \á "♜" \b "♜" \c "♜" \d "♜" \e "♜" \é "♜" \f "♜" \g	"♜" \h "♜"
                                          \i "♞" \í "♞" \j "♞" \k "♞" \l "♞" \m "♞" \n "♞" \ñ "♞" \o "♞" \ó "♞"
                                          \p "♝" \q "♝" \r	"♝" \s "♝" \t	"♝" \u "♝" 	\ú "♝"  \ü "♝"  \v "♝"	\w "♝"
                                          \x "♛" \y "♛"  \z "♛"	\0 "♛"	\1 "♛"	\2 "♛" \3 "♛" \4 "♛" \! "♛"	\" "♛"
                                          \# "♚" \$ "♚" \% "♚" \& "♚" \' "♚"	\( "♚"	\) "♚"	\* "♚"	\+ "♚"	\, "♚"
                                          \- "♖"	\. "♖" \/ "♖"	\: "♖" \;	"♖" \< "♖"	\= "♖"	\> "♖"	\? "♖"	\@ "♖"
                                          \[ "♘"	\\ "♘" \] "♘"	\^ "♘" \_	"♘" \` "♘" \{ "♘"	\| "♘"	\} "♘"	\~ "♘"
                                          \5 "♗"	\6 "♗"	\7 "♗"	\8 "♗"	\9 "♗"
                                          \A "♗" \Á "♗" \B "♗" \C "♗" \D "♗" \E "♕" \É "♕" \F "♕" \G "♕" \H "♕"
                                          \I "♕" \Í "♕" \J "♕" \K "♕" \L "♕" \M "♔" \N "♔" \Ñ "♔" \O "♔" \Ó "♔"
                                          \P "♔" \Q "♔" \R  "♔" \S "♔" \T "♔" \U "♖"  \Ú "♘" \Ü "♗"  \V "♕"  \W "♔"
                                          \X "♜" \Y "♞"  \Z "♝" \space " " \newline "\n"} caracter)] (if (nil? y) caracter (val y))))

(defn función-coordx [caracter] (let [y (find
                                         {\a "♜" \á "♞"        \b "♝"         \c "♛" \d "♚"	\e "♖"        \é "♘"         \f "♗"        \g "♕"	\h "♔"
                                          \i "♜"  \í "♞"        \j "♝"         \k "♛" \l "♚" \m "♖"        \n "♘"         \ñ "♗"        \o "♕"  \ó "♔"
                                          \p "♜"  \q "♞"        \r "♝"         \s "♛" \t "♚"	\u "♖"  \ú "♘" \ü "♗"  \v "♕"  \w "♔"
                                          \x "♜"  \y "♞"  \z "♝" 	\0 "♛" \1 "♚"	\2 "♖"        \3 "♘"	       \4 "♗"	       \! "♕"	\" "♔"
                                          \# "♜"  \$ "♞"        \% "♝"         \& "♛" \' "♚"	\( "♖"        \) "♘"	       \* "♗"	  \+ "♕"	\, "♔"
                                          \- "♜"   \. "♞"        \/ "♝"         \: "♛" \; "♚"	\< "♖"      	\= "♘"	       \> "♗"	  \? "♕"	\@ "♔"
                                          \[ "♜"	 \\ "♞"        \] "♝"         \^ "♛" \_	"♚" \` "♖"	      \{ "♘"	       \| "♗"	   \} "♕"	\~ "♔"
                                          \5 "♜"	 \6	"♞"        \7 "♝"         \8 "♛" \9 "♚"
                                          \A "♖"    \Á "♘"        \B "♘"         \C "♕" \D "♔" \E "♜"        \É "♞"         \F "♝"     \G "♛"  \H "♚"
                                          \I "♖"    \Í "♘"        \J "♗"         \K "♕" \L "♔" \M "♜"        \N "♞"         \Ñ "♝"        \O "♛"  \Ó "♚"
                                          \P "♖"    \Q "♘"        \R "♗"         \S "♕" \T "♔" \U "."  \Ú "." \Ü "."  \V "."  \W "."
                                          \X "."  \Y "."  \Z "." \space " " \newline " "} caracter)] (if (nil? y) (str " " caracter) (str (función-coordy caracter) (val y)))))

(defn función-decif-coord [texto] (let [deci (find {"♜♜" \a "♜♞" \á "♜♝" \b "♜♛" \c "♜♚" \d "♜♖" \e "♜♘" \é "♜♗" \ƒ "♜♕" \g "♜♔" \h
                                                    "♞♜" \i "♞♞" \í "♞♝" \j "♞♛" \k "♞♚" \l "♞♖" \m "♞♘" \n "♞♗" \ñ "♞♕" \o "♞♔" \ó
                                                    "♗♖" \A  "♗♘" \Á   "♗♗" \B   "♗♕" \C "♗♔" \D
                                                    "♕♜" \E "♕♞" \É  "♕♝" \F  "♕♛"  \G  "♕♚" \H "♕♖" \I  "♕♘" \Í   "♕♗" \J "♕♕"  \K  "♕♔" \L
                                                    "♔♜" \M "♔♞" \N  "♔♝"  \Ñ  "♔♛" \O "♔♚"  \Ó "♔♖"  \P "♔♘" \Q  "♔♗"  \R  "♔♕" \S "♔♔" \T
                                                    "♖." \U "♘." \Ú "♗." \Ü "♕." \V "♔." \W "♜." \X "♞." \Y "♝." \Z
                                                    "♚♜" \# "♚♞" \$ "♚♝" \% "♚♛" \' "♚♚" \( "♚♖" \) "♚♘" \* "♚♗" \+ "♚♔" \, "♛♔" \"
                                                    "♖♜" \- "♖♞" \. "♖♝" \/ "♖♛" \: "♖♚" \; "♖♖" \< "♖♘" \= "♖♗" \> "♖♕" \? "♖♔" \@
                                                    "♘♜" \[ "♘♞" \\ "♘♝" \] "♘♛" \^ "♘♚" \_ "♘♖" \` "♘♘" \{ "♘♗" \| "♘♕" \} "♘♔" \~ "♚♕" \+
                                                    "♗♜"  \5 "♗♞" \6 "♗♝" \7 "♗♛" \8 "♗♚" \9  "♛♛" \0 "♛♚" \1 "♛♖" \2 "♛♘" \3 "♛♗" \4 "♛♕" \!
                                                    "♝♜"  \p "♝♞" \q "♝♝" \r "♝♛" \s "♝♚" \t "♝♖" \u  "♝♘" \ú "♝♗" \ü "♝♕" \v "♝♔" \w
                                                    "♛♜" \x "♛♞" \y "♛♝" \z "  " \space "\n " \newline} (str texto))] (if (nil? deci) texto (val deci))))

(defn función-cifrado-cup [texto]
  (let [cifrado (map función-coordx texto)]
    (clojure.string/join cifrado)))

(defn función-decifrate-1
  [texto] (función-decif-coord (clojure.string/join (take 2 texto))))

(defn función-decifrate-2 [elemento] (map función-decifrate-1 elemento))

(defn función-cifrar [leer escribir]
  (spit escribir (función-cifrado-cup (slurp leer))))

(defn función-descifrar [leer escribir]
  (spit escribir (clojure.string/join (función-decifrate-2 (partition-all 2 (slurp leer))))))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (if (empty? args)
    (println "Error.")
    (if (= "polibio" (get (vec args) 0))
      (if (= "cifrado" (get (vec args) 1))
        (función-cifrar (get (vec args) 2) (get (vec args) 3))
        (función-descifrar (get (vec args) 2) (get (vec args) 3)))
      (if (= "cifrado" (get (vec args) 1))
        (plf08.columnarsimple/cifrar-texto (get (vec args) 2) (slurp (get (vec args) 3)) (get (vec args) 4))
        (plf08.columnarsimple/descifrar-texto (get (vec args) 2) (slurp (get (vec args) 3)) (get (vec args) 4))))))


