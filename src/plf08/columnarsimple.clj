(ns plf08.columnarsimple
  (:require clojure.string))

(defn orden-char
  [caracter]
  (let [x ({\a 1 \á 2 \b 3 \c 4 \d 5 \e 6 \é 7 \f 8 \g 9 \h 10 \i 11 \í 12 \j 13 \k 14 \l 15
            \m 16 \n 17 \ñ 18 \o 19 \ó 20 \p 21 \q 22 \r 23 \s 24 \t 25 \u 26 \ú 27 \ü 28 \v 29 \w 30
            \x 31 \y 32 \z 33 \0 34 \1 35 \2 35 \3 36 \4 37 \! 38 \" 39 \# 40 \$ 41 \% 42 \& 43 \' 44
            \( 46 \) 47 \* 48 \+ 49 \, 50 \- 51 \. 52 \/ 53 \: 54 \; 55 \< 56 \= 57 \> 58 \? 59 \@ 60
            \[ 61 \\ 62 \] 63 \^ 64 \_ 65 \` 66 \{ 67 \| 68 \} 69 \~ 70 \5 71 \6 72 \7 73 \8 74 \9 75
            \A 1 \Á 2 \B 3 \C 4 \D 5 \E 6 \É 7 \F 8 \G 9 \H 10 \I 11 \Í 12 \J 13 \K 14 \L 15
            \M 16 \N 17 \Ñ 18 \O 19 \Ó 20 \P 21 \Q 22 \R 23 \S 24 \T 25 \U 26 \Ú 27 \Ü 28 \V 29 \W 30
            \X 31 \Y 32 \Z 33} caracter)] x))

(defn ordenar-valores-posiciones
  [clave]
  (sort-by val < (zipmap (range (count clave)) (map orden-char clave))))

(defn obtener-posiciones-2
  [clave]
  (map (fn [col] (get col 0)) (ordenar-valores-posiciones clave)))

(defn agrupar-texto
  [clave texto]
  (partition-all (count clave) texto))

(defn cifrar-1
  [particiones orden]
  (map (fn [parti] (map (fn [pos] (nth parti pos " ")) orden)) particiones))

(defn cifrar-descifrar
  [clave coleccion]
  (let [x (map (fn [pos] (map (fn [parti] (nth parti pos "")) coleccion)) (range (count clave)))]
    (clojure.string/join (flatten x))))


;descifrar
;    


(defn pos-orden
  [clave]
  (map (fn [llave]
         ((zipmap (ordenar-valores-posiciones clave) (range (count clave))) llave)) (map (fn [a] (vector (first a) (last a))) (partition-all 2 (flatten (map vector (zipmap (range (count clave)) (map orden-char clave))))))))

(defn ordenar-texto-cif
  [texto nump clave]
  (map (fn [pos] (nth (partition-all nump texto) pos)) (pos-orden clave)))

(defn num-particiones
  [tamclave tamtxt]
  (if (int? (/ tamtxt tamclave)) (/ tamtxt tamclave) (inc (int (/ tamtxt tamclave)))))

(defn cifrar-texto
  [clave textor textow]
  (spit textow (cifrar-descifrar clave (cifrar-1 (agrupar-texto clave textor) (obtener-posiciones-2 clave)))))

(defn descifrar-texto
  [clave textocif escribir]
  (spit escribir (cifrar-descifrar clave (ordenar-texto-cif textocif (num-particiones (count clave) (count textocif)) clave))))