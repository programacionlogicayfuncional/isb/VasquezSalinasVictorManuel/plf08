# Aplicacion de cifrado y descifrado por medio de cuadro de Polibio y Transposicion columnar simple 

## Descripción del proyecto

La aplicación tiene la capacidad de cifrar el contenido de un archivo de texto  
siguiendo el metodo de cuadradro de polibio. El texto cifrado se guarda en  
un nuevo archivo de texto. La aplicación puede tomar el archivo de texto  
donde esta contenido el cifrado y descifralo dando como resultado un nuevo archivo  
de texto en el que estará el contenido del archivo que se cifro.

La aplicación por otra parte, pude realizar tambien el cifrado por medio del  
metodo de transposicion columnar simple. El contenido de un archivo de texto
es leído, cifrado y guardado en nuevo archivo de texto. Por ultimo, la aplicación  
puede leer un archivo de texto que se haya cifrado y descifrarlo en un nuevo archivo  
de texto, dando como resultado el contenido original del archivo que se cifro.  

## Forma de ejecutar

La aplicación se ejecuta desde la terminal del sistema operativo y admite 2 series  
de argumentos, una serie para el cifrado y descifrado por el método cuadrado de polibio  
y otra serie para el cifrado y descifrado por el método de trasnposicion columnar simple.  
Ambas series estan precedidas por el comando *lein run*: 

1. Serie 1: cifrar y descifrar por el método cuadrado de polibio.
    - Nombre del método para cifrar o descifrar.
    - Acción a realizar.
    - Nombre del archivo de texto a cifrar.
    - Nombre del archivo de texto donde se guardara el cifrado.
2. Serie 2: cifrar y descifrar por el método de trasnposicion columnar simple.
    - Nombre del método para cifrar o descifrar.
    - Acción a realizar, cifrado o descifrado.
    - Palabra clave para el cifrado.
    - Nombre del archivo de texto a cifrar.
    - Nombre del archivo de texto donde se guardara el cifrado.  

## Opciones para la eejcucion

Para ejecutar la aplicación, se debe abrir una nueva terminal del sistema operativo  
y entrar a la carpeta donde esta el proyecto. Ahora se debe escribir el comando  
*lein run* seguido de alguna de las 2 series de argumentos. Los argumentos deben seguir las  
siguientes especificaciones:

- Todos deben estar encerrados por comillas dobles.
- El argumento *Nombre del método para cifrar o descifrar* tiene dos valores validos:
    - "polibio", para cifrar o descifrar por el metodo de cuadrado de Polibio.
    - "columnar", para cifrar o descifrar por el metodo de transposicion columnar simple. 
- El argumento *Palabra clave para el cifrado* debe ser de longitud mayor o igual a 8 caracteres.
- El argumento *Acción a realizar* tiene dos valores validos:
    - "cifrado", para cifrar el contenido de un archivo.
    - "descifrado", para descifrar el contenido de un archivo.
- El argumento *Nombre del archivo de texto a cifrar* debe especificar la ruta donde esta alojado dicho archivo.
- El argumento *Nombre del archivo de texto donde se guardara el cifrado* debe especificar la ruta donde se alojara el archivo.

## Ejemplos de ejecución

A continuacion se presenta un ejemplo por cada accion que puede realizar la  
aplicación, esto es, cifrado y descifrado mediante cuadrado de Polibio y  
cifrado y descifrado mediante transposicion columnar simple.

### Cifrado y descifrado mediante cuadrado de Polibio

Para cifrar mediante cuadrado de polibio, en la terminal del sistema opeativo se tiene  
que escribir lo siguiente:

- lein run "polibio" "cifrado" "resources/ITO.txt" "resources/itocifpolibio.txt"

Esta ejecución creara el archivo *itocifpolibio.txt* en la capeta *resources* del  
proyecto con el cifrado del contenido del archivo *ITO.txt*.

Para descifrar mediante cuadrado de polibio, en la terminal del sistema opeativo se tiene  
que escribir lo siguiente:

- lein run "polibio" "descifrado" "resources/itocifpolibio.txt" "resources/itodecifpolibio.txt"

Esta ejecución dejara el archivo *itodecifpolibio.txt* en la capeta *resources* del  
proyecto con el cifrado del contenido del archivo *itocifpolibio.txt*

### Cifrado y descifrado mediante transposicion columnar simple

Para cifrar mediante transposicion columnar simple en la terminal del sistema opeativo se tiene  
que escribir lo siguiente:

- lein run "columnar" "cifrado" "8-letras" "resources/columnar1.txt" "resources/cifcolumnar.txt"

Esta ejecución dejara el archivo *cifcolumnar.txt* en la capeta *resources* del  
proyecto con el cifrado del contenido del archivo *columnar1.txt*

Para descifrar mediante transposicion columnar simple en la terminal del sistema opeativo se tiene  
que escribir lo siguiente:

- lein run "columnar" "descifrado" "8-letras" "resources/cifcolumnar.txt" "resources/decifcolumnar.txt"

Esta ejecución dejara el archivo *decifcolumnar.txt* en la capeta *resources* del  
proyecto con el cifrado del contenido del archivo *cifcolumnar.txt*

## Limitaciones y particularidades 

Al desarrollar la aplicación se hallaron situaciones problematicas y sus soluciones dieron un resultado  
ligeramente diferente al esperado, sin embargo, esto no afecta al objetivo principal: cifrar y descifrar  
sin perdida de informacion.

La primer problematica se dio desarrollando el cifrado y descifrado mediante cuadrado de polibio. 
El numero de casillas disponibles a partir de los caracteres para filas y columnas es de 100, 
mientras que los caracteres posibles a cifrar es de 108. Una solucion posible era que algunos  
caracteres compartieran su casilla con los caracteres faltantes de ella, sin embargo esta solucion 
genero un nuevo problema. Cuando se cifra, los caracteres que comparten casilla tienen la misma combinacion
*columna-casilla* y al momento de descifrar, no es posible saber que caracter se cifró. Por ejemplo,  
el caracter *X* y el caracter *W* comparten la misma combinacion *columna-casilla*, se cifra correctamente  
pero al momento de descifrar no es posible saber si la combinacion pertence al caracter *X* o al  
caracter *W* porque evidentemente en el descifrado no hay nocion del archivo que se cifro. 
La solucion fue agregar un simbolo que diferencie a los 8 caracteres faltantes de casillas para que
las combinaciones *columna-casilla* nunca se repitan.

La segunda problematica se presento al desarrollar el cifrado y descifrado mediante trasnposicion
columnar simple. En alguno punto del proceso para cifrar se necesita dividir el texto  
a cifrar en n veces (n es igual al tamaño de la palbra clave). En una situacion ideal, el resultado de  
dividir el tamaño del texto entre el tamaño de la palabra clave es 0. Cuando el resultado es 0, todas las  
divisiones del texto a cifrar contienen el mismo numero de elementos, sin embargo, esto es poco probable  
que ocurra. Cuando el resultado no es 0, unicamente la ultima division del texto a cifrar es menor a n, por ello se tuvo  
que *completar*. Si no se completa, en algun punto se tendra un *IndexOutOfBoundsException* porque el proceso  
tratara de obtener un elemento de la ultima division y dicho elemento no existe. Se decidio *completar* la ultima 
division con espacios en blanco para evitar alterar el texto que se cifró.
